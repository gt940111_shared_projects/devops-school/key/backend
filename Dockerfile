FROM openjdk:8-alpine

COPY /build/libs/*.jar devschool-backend.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "devschool-backend.jar"]